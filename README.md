#Routes in Maps

This is a coding challenge developed during the Apple Developer Academy Foundations course. The objective is to trace all the routes to a destination on the map. The destination is defined by touching a point on the map.
