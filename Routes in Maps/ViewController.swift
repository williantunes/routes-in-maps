//
//  ViewController.swift
//  Routes in Maps
//
//  Created by Willian Antunes on 7/16/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    
    @IBOutlet weak var myMapView: MKMapView!
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myMapView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if myMapView.annotations.count > 0 {
                myMapView.removeAnnotation(myMapView.annotations[0])
                for overlay in myMapView.overlays {
                    myMapView.removeOverlay(overlay)
                }
            }
            let position = touch.location(in: view)
            let coord = myMapView.convert(position, toCoordinateFrom: view)
            
            let pin = MKPointAnnotation()
            pin.coordinate = coord
            pin.title = "Destination"
            myMapView.addAnnotation(pin)
            
            traceRoute(From: locationManager.location!.coordinate, To: coord)
        }
    }
    
    // MARK: - Trace route
    
    func traceRoute(From: CLLocationCoordinate2D, To: CLLocationCoordinate2D) {
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: From))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: To))
        request.requestsAlternateRoutes = true
        request.transportType = .automobile
        let directions = MKDirections(request: request)
        directions.calculate { (response, error) in
            if let routes = response?.routes {
                for route in routes {
                    self.myMapView.addOverlay(route.polyline)
                    self.myMapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
                }
            }
        }
    }
    
    // MARK: - Check location authorization status

    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            myMapView.showsUserLocation = true
            if let location = locationManager.location {
                
                var region = MKCoordinateRegion()
                region.center.latitude = location.coordinate.latitude
                region.center.longitude = location.coordinate.longitude
                region.span.longitudeDelta = 0.015
                
                myMapView.setRegion(region, animated: true)
            }
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }

}

extension ViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 3
        return renderer
    }
    
}

